import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;

@SpringBootConfiguration
public class MainUsingLogger {

    // Creating logger
    private static final Logger LOG = LogManager.getLogger(MainUsingLogger.class);

    // Always use static string variable for storing logger messages
    private static final String LOG_MESSAGE_INFO = "This is a log INFO message";
    private static final String LOG_MESSAGE_WARNING = "We throwing an illegal argument exception!";
    private static final String EXCEPTION_MESSAGE = "Exception is happened: ";
    private static final String EXCEPTION_ARGUMENT_MESSAGE = "Some exception message";

    public static void main(String[] args) {
        SpringApplication.run(MainUsingLogger.class, args);

        LOG.info(LOG_MESSAGE_INFO);

        try {
            LOG.warn(LOG_MESSAGE_WARNING);
            throw new  IllegalArgumentException(EXCEPTION_ARGUMENT_MESSAGE);
        } catch (IllegalArgumentException e) {
            LOG.error(EXCEPTION_MESSAGE+ e.getLocalizedMessage());
        }
    }
}
